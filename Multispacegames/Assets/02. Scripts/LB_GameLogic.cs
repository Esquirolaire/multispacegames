﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LB_GameLogic : MonoBehaviour
{

	public Text lblBest;
	public Text lblTime;
	public Text lblLife;

	public GameObject prefab_Bomb;
	private List<Object> bombs = new List<Object> ();
	public GameObject lluna;
	public float initialTime;

	private float bestScore;
	private float timer;
	private float timeToAppear;

	[HideInInspector]
	public int lifes;

	private AudioSource audioSource;
	public AudioClip planetExplosion;

	public GameObject prefabExplosion;
	public GameObject prefabParticles;

	public GameObject panelGameOver;

	float totalTime = 0.0f;

	bool planetDestroyed;

	private const string LB_BEST_SCORE = "LB_BestScore";

	// Use this for initialization
	void Start ()
	{
		panelGameOver.SetActive (false);
		timeToAppear = initialTime;
		timer = 0.0f;
		lifes = 1;
		planetDestroyed = false;

		bestScore = PlayerPrefs.GetInt (LB_BEST_SCORE, 0);

		// Look for audiosource in main camera
		audioSource = Camera.main.GetComponent <AudioSource> ();
		if (audioSource == null) {
			Debug.LogError ("Error finding Audio Source in main camera");
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		UpdateScores ();

		if (lifes < 1 && !planetDestroyed) {

			lluna.SetActive (false);

			Instantiate (prefabExplosion, lluna.transform.position, Quaternion.identity);
			Instantiate (prefabParticles, lluna.transform.position, Quaternion.identity); 
			audioSource.PlayOneShot (planetExplosion);
			DestroyAllBombs ();
			planetDestroyed = true;
			Invoke ("GameOver", 2.0f);

		} else if (lifes > 0) {

			totalTime += Time.deltaTime;
			timer += Time.deltaTime;
			GenerateBomb ();
		}
	}

	void GameOver ()
	{
		if (totalTime > bestScore) {
			bestScore = totalTime;
			PlayerPrefs.SetInt (LB_BEST_SCORE, (int)bestScore);
		}

		panelGameOver.SetActive (true);
	}

	void UpdateScores ()
	{
		lblTime.text = "Time: " + (int)totalTime;
		lblBest.text = "Best Score: " + (int)bestScore;

		if (lifes >= 0) {
			lblLife.text = "Lifes: " + lifes;
		}
	}

	void GenerateBomb ()
	{
		if (timer >= timeToAppear) {
			Vector3 randPos = Random.onUnitSphere;
			bombs.Add (Instantiate (prefab_Bomb, lluna.transform.position + randPos * 1.5f, Quaternion.identity));
			timeToAppear -= .1f;
			timer = 0.0f;
			Debug.Log ("timer: " + timeToAppear);
		}
	}

	void DestroyAllBombs ()
	{
		foreach (GameObject bomb in bombs) {
			Destroy (bomb);
		}
	}
}
