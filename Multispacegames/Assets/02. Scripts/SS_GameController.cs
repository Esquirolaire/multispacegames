﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SS_GameController : MonoBehaviour
{

	public Text labelInfo;
	public GameObject[] buttons;

	private StateMachine m_SM;
	//	public GameObject ;

	// Use this for initialization
	void Start ()
	{
		closeAllLights ();
		m_SM.ChangeState (SimonSays_State.WaitingToPlay);
	}

	void closeAllLights ()
	{
		foreach (GameObject button in buttons) {
			Light buttonLight = button.GetComponent<Light> ();
			buttonLight.enabled = false;
		}
	}

	public void ButtonPressed (int index)
	{
		if (index >= buttons.Length) {
			Debug.LogError ("Light index out of bounds!");
		} else {
			buttons [index].GetComponent<Light> ().enabled = true;
		}
	}

	public struct StateMachine
	{
		public SimonSays_State m_eState;
		public float m_fTime;
		public bool m_bIsFirstTime;


		public void ChangeState (SimonSays_State _state)
		{
			m_eState = _state;
			m_bIsFirstTime = true;
			m_fTime = 0f;
		}

		public bool IsFirstTime ()
		{
			bool isFirstTime = m_bIsFirstTime;
			m_bIsFirstTime = false;
			return isFirstTime;
		}


		public void Update ()
		{
			m_fTime += Time.deltaTime;
		}
	}

	public enum SimonSays_State
	{
		WaitingToPlay = 0,
		IncreasingSequence,
		PlayingSequence,
		WaitingUserInput,
		UserInputOK,
		UserInputFAIL
	}

	void Update ()
	{
		UpdateStateMachine ();
	}


	void UpdateStateMachine ()
	{
		m_SM.Update ();
		switch (m_SM.m_eState) {
		case SimonSays_State.WaitingToPlay:
			{
				if (m_SM.IsFirstTime ()) {
					labelInfo.text = "Press S to Start";
				}

				if (Input.GetKeyDown (KeyCode.S)) {
					m_SM.ChangeState (SimonSays_State.IncreasingSequence);
				}
			}
			break;
		case SimonSays_State.IncreasingSequence:
			{
				if (m_SM.IsFirstTime ()) {
					//TODO incrementar secuencia
					m_SM.ChangeState (SimonSays_State.PlayingSequence);
				}   

			}
			break;
		case SimonSays_State.PlayingSequence:
			{
				if (m_SM.IsFirstTime ()) {
					labelInfo.text = "Playing sequence";
				}    
			}
			break;
		case SimonSays_State.WaitingUserInput:
			{   
				if (m_SM.IsFirstTime ()) {
				}    
			}
			break;
		case SimonSays_State.UserInputOK:
			{
				if (m_SM.IsFirstTime ()) {
				}    
			}
			break;

		case SimonSays_State.UserInputFAIL:
			{
				if (m_SM.IsFirstTime ()) {
				}    
			}
			break;
		}
	}
}
