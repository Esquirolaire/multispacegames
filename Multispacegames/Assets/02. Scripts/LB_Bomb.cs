﻿using UnityEngine;
using System.Collections;

public class LB_Bomb : MonoBehaviour
{
	public float timeToDestroy = 3.0f;
	private float timeAcumulator;
	private LB_GameLogic logic;

	public GameObject prefabExplosion;
	private AudioSource audioSource;

	public AudioClip deactivateBomb;
	public AudioClip explodeBomb;

	// Use this for initialization
	void Start ()
	{
		GameObject o = GameObject.FindGameObjectWithTag ("GameLogic");
		if (o == null) {
			Debug.LogError ("Error loading Game Logic Component");
		} else {
			logic = o.GetComponent<LB_GameLogic> ();
		}

		// Look for audiosource in main camera
		audioSource = Camera.main.GetComponent <AudioSource> ();
		if (audioSource == null) {
			Debug.LogError ("Error finding Audio Source in main camera");
		}
			
		Invoke ("DestroyBomb", timeToDestroy);
		timeAcumulator = 0f;
	}
	
	// Update is called once per frame
	void Update ()
	{
		timeAcumulator += Time.deltaTime;
		Color l = Color.Lerp (Color.green, Color.red, timeAcumulator / timeToDestroy);
		GetComponent <MeshRenderer> ().material.color = l;
	}

	void DestroyBomb ()
	{
		audioSource.PlayOneShot (explodeBomb);
		logic.lifes--;
		Destroy (this.gameObject);
	}

	void OnMouseDown ()
	{
		audioSource.PlayOneShot (deactivateBomb);
		Destroy (this.gameObject);
	}
}
