﻿using UnityEngine;
using System.Collections;
using FSMHelper;

public class SimonSaysState_UserInputFAIL : BaseFSMState
{

	private SimonSaysBehaviour m_ssBehaviour;
	private float timer;
	public float showMessageTime = 3.0f;

	public override void Enter ()
	{
		Debug.Log ("Enter User Input Fail");
		SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine ();
		m_ssBehaviour = SM.m_GameObject.GetComponentsInChildren<SimonSaysBehaviour> () [0];
		m_ssBehaviour.info.text = "Conec nens de parvulari que ho farien millor.";

		timer = 0f;

	}

	public override void Exit ()
	{
		Debug.Log ("Exit User Input Fail");

	}

	public override void Update ()
	{
		Debug.Log ("Update User Input Fail");

		timer += Time.deltaTime;

		if (timer > showMessageTime) {
			DoTransition (typeof(SimonSaysState_WaitingToPlay));
		}
	}
}