﻿using UnityEngine;
using System.Collections;
using FSMHelper;

public class SimonSaysState_UserInputOK : BaseFSMState
{

	private SimonSaysBehaviour m_ssBehaviour;
	private float timer;
	public float showMessageTime = 3.0f;

	public override void Enter ()
	{
		Debug.Log ("Enter User Input Ok");

		SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine ();
		m_ssBehaviour = SM.m_GameObject.GetComponentsInChildren<SimonSaysBehaviour> () [0];
		m_ssBehaviour.info.text = "Guay! Fem-ho més complicat!";

		timer = 0f;
	}

	public override void Exit ()
	{
		Debug.Log ("Exit User Input Ok");

	}

	public override void Update ()
	{
		timer += Time.deltaTime;

		if (timer > showMessageTime) {
			DoTransition (typeof(SimonSaysState_IncreasingSequence));
			Debug.Log ("Update User Input Ok");
		}
	}
}
