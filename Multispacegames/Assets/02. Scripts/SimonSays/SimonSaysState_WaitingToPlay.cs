﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using FSMHelper;

public class SimonSaysState_WaitingToPlay : BaseFSMState
{

	private SimonSaysBehaviour m_ssBehaviour;

	public override void Enter ()
	{
		Debug.Log ("Enter WaitingToPlay");

		SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine ();
		m_ssBehaviour = SM.m_GameObject.GetComponentsInChildren<SimonSaysBehaviour> () [0];
		m_ssBehaviour.closeAllLights ();
		m_ssBehaviour.info.text = "Apreta S per començar.";
	}

	public override void Exit ()
	{
		Debug.Log ("Exit WaitingToPlay");
		//Initialize variables for new game
		m_ssBehaviour.sequence.Clear ();
		m_ssBehaviour.buttonTreated = true;
		m_ssBehaviour.timer = 0f;
	}

	public override void Update ()
	{
		Debug.Log ("Update WaitingToPlay");

		if (Input.GetKeyDown (KeyCode.S)) {
			DoTransition (typeof(SimonSaysState_IncreasingSequence));
		}
	}
}