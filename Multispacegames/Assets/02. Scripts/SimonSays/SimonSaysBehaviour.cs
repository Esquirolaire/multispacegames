﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using FSMHelper;

public class SimonSaysBehaviour : MonoBehaviour
{

	// keep an instance of our state machine
	private SimonSaysBehaviourStateMachine m_SimonSaysSM = null;

	//Public variables_UnityEditor:
	public GameObject[] buttons;
	public AudioClip[] sounds;
	public Text info;

	private AudioSource audioSource;

	[HideInInspector]
	public List<int> sequence = new List<int> ();
	[HideInInspector]
	public int buttonPressed;
	[HideInInspector]
	public bool buttonTreated;

	public float timeOn = 2f;
	public float timeOff = .5f;
	public float timer;
	private bool isLightOn;

	//Create state machine and start it
	void Start ()
	{
		m_SimonSaysSM = new SimonSaysBehaviourStateMachine (this.gameObject);
		m_SimonSaysSM.StartSM ();

		// Look for audiosource in main camera
		audioSource = Camera.main.GetComponent <AudioSource> ();

		if (audioSource == null) {
			Debug.LogError ("Error finding Audio Source in main camera");
		}

		buttonTreated = true;
		isLightOn = false;
		timer = 0f;
	}
	
	// Update is called once per frame
	void Update ()
	{
		// update the state machine every frame
		m_SimonSaysSM.UpdateSM ();

		//this is how yo can print the current actvie state tree to the log for debugging
		if (Input.GetKeyDown (KeyCode.Space)) {
			m_SimonSaysSM.PrintActiveStateTree ();
		}
	}

	// stop the state machine to ensure all the Exit() gets called
	void OnDestroy ()
	{
		if (m_SimonSaysSM != null) {
			m_SimonSaysSM.StopSM ();
			m_SimonSaysSM = null;
		}
	}

	public void ClickOnButton (int index)
	{

		if (buttonTreated) {
			buttonPressed = index;
			buttonTreated = false;
		}
	}

	public void PressButtonWithTimeLimit (int index)
	{
		if (index >= buttons.Length) {
			Debug.LogError ("PressButton: index(" + index + ") més gran que el número de butons.");
			return;
		}

		closeAllLights ();

		Light buttonLight = buttons [index].GetComponent <Light> ();
		buttonLight.enabled = true;
		audioSource.PlayOneShot (sounds [index]);
	}

	//Returns true when light on time is
	public bool PressButtonWithTimeLimit (int index, float deltaTime)
	{
		bool isFinished = false;
		timer += deltaTime;

		if (timer < timeOn && !isLightOn) {

			//Debug.Log ("TIME ON : " + timer);
			PressButtonWithTimeLimit (index);
			isLightOn = true;

		} else if (timer >= timeOn && timer < (timeOn + timeOff) && isLightOn) {

			//Debug.Log ("TIME OFF: " + timer);
			closeAllLights ();
			isLightOn = false;
		
		} else if (timer > (timeOn + timeOff)) {

			//Debug.Log ("FINISH: " + timer);
			isFinished = true;
			timer = 0f;
			buttonTreated = true;
		}

		return isFinished;
	}

	public void closeAllLights ()
	{
		foreach (GameObject button in buttons) {
			Light buttonLight = button.GetComponent<Light> ();
			buttonLight.enabled = false;
		}
	}
}
