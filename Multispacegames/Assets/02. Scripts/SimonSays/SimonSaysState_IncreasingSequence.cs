﻿using UnityEngine;
using System.Collections;
using FSMHelper;

public class SimonSaysState_IncreasingSequence : BaseFSMState
{

	private SimonSaysBehaviour m_ssBehaviour;

	public override void Enter ()
	{
		Debug.Log ("Enter Increasing sequence");

		SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine ();
		m_ssBehaviour = SM.m_GameObject.GetComponentsInChildren<SimonSaysBehaviour> () [0];
		m_ssBehaviour.info.text = "Vist i no vist. Incrementant seqüència.";
	
	}

	public override void Exit ()
	{
		Debug.Log ("Exit Increasing sequence");

	}

	public override void Update ()
	{
		Debug.Log ("Update Increasing sequence");

		int newStep = Random.Range (0, 4);
		m_ssBehaviour.sequence.Add (newStep);

		DoTransition (typeof(SimonSaysState_PlaySequence));
	}
}
