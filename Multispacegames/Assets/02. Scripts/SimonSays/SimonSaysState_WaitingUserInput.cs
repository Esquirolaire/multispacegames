﻿using UnityEngine;
using System.Collections;
using FSMHelper;

public class SimonSaysState_WaitingUserInput : BaseFSMState
{
	public float timeOn = .5f;
	public float timeOff = .5f;
	public float timer;

	private SimonSaysBehaviour m_ssBehaviour;

	private int currentSeqNumber;

	public override void Enter ()
	{
		Debug.Log ("Enter Waiting User Input");

		SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine ();
		m_ssBehaviour = SM.m_GameObject.GetComponentsInChildren<SimonSaysBehaviour> () [0];
		m_ssBehaviour.info.text = "Et toca.";

		m_ssBehaviour.closeAllLights ();
		currentSeqNumber = 0;
	}

	public override void Exit ()
	{
		Debug.Log ("Exit Waiting User Input");
	}

	public override void Update ()
	{
		Debug.Log ("Update Waiting User Input");

		if (!m_ssBehaviour.buttonTreated && currentSeqNumber < m_ssBehaviour.sequence.Count) {

			Debug.Log ("m_ssBehaviour.sequence [currentSeqNumber] : " + m_ssBehaviour.sequence [currentSeqNumber]);
			Debug.Log ("m_ssBehaviour.buttonPressed : " + m_ssBehaviour.buttonPressed);

			if (m_ssBehaviour.sequence [currentSeqNumber] == m_ssBehaviour.buttonPressed) {

				if (m_ssBehaviour.PressButtonWithTimeLimit (m_ssBehaviour.buttonPressed, Time.deltaTime)) {
					m_ssBehaviour.buttonTreated = true;
					currentSeqNumber++;
				} 
			} else {
				DoTransition (typeof(SimonSaysState_UserInputFAIL));
			}

		} else if (currentSeqNumber >= m_ssBehaviour.sequence.Count) {
			DoTransition ((typeof(SimonSaysState_UserInputOK)));
		}
	}
}
