﻿using UnityEngine;
using System.Collections;
using FSMHelper;

public class SimonSaysState_PlaySequence : BaseFSMState
{
	private SimonSaysBehaviour m_ssBehaviour;

	private int currentSeqNumber;

	public override void Enter ()
	{
		//Debug.Log ("Enter Playing sequence");

		SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine ();
		m_ssBehaviour = SM.m_GameObject.GetComponentsInChildren<SimonSaysBehaviour> () [0];
		m_ssBehaviour.info.text = "Armando rampas. Reproduint seqüència.";

		currentSeqNumber = 0;
	}

	public override void Exit ()
	{
		//Debug.Log ("Exit Playing sequence");
	}

	public override void Update ()
	{
		//Debug.Log ("Update Playing sequence");
			
		if (currentSeqNumber < m_ssBehaviour.sequence.Count) {

			if (m_ssBehaviour.PressButtonWithTimeLimit (m_ssBehaviour.sequence [currentSeqNumber], Time.deltaTime)) {
				currentSeqNumber++;
			}

		} else {
			DoTransition (typeof(SimonSaysState_WaitingUserInput));
		}
	}
}
