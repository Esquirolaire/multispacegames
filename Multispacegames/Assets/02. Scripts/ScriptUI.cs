﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ScriptUI : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void goToLunarBomb ()
	{
		SceneManager.LoadScene ("Scene_LunarBomb");
	}

	public void goToTicTacToe ()
	{
		SceneManager.LoadScene ("Scene_TicTacToe");
	}

	public void goToSimonSays ()
	{
		SceneManager.LoadScene ("Scene_SimonSays");
	}

	public void goToBasicLayout ()
	{
		SceneManager.LoadScene ("Scene_BasicLayout");
	}

	public void goBackToMenu ()
	{
		SceneManager.LoadScene ("Scene_Menu");
	}

	public void ExitGame ()
	{
		#if UNITY_EDITOR 
		UnityEditor.EditorApplication.isPlaying = false;
		#elif UNITY_STANDALONE 
			Application.Quit ();
		#endif
	}
}
