﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour
{

	public float speedRotation = 0.1f;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		float rotation = Time.deltaTime * speedRotation;
		transform.Rotate (0f, rotation, 0f);
	}
}
